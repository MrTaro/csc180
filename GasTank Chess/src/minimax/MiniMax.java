package minimax;

import game.board.Board;
import game.board.pieces.Bishop;
import game.board.pieces.King;
import game.board.pieces.Knight;
import game.board.pieces.Piece;
import game.board.pieces.Queen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.Timer;

/**
 * MiniMax class
 * <p>
 * When created, it will do the minimax for the current board state
 * <p>
 * The best move is created and can be accessed by a getter function
 * 
 * @author Anthony Nguyen
 *
 */
public class MiniMax {
	// Variables
	private Board gameBoard;
	private Piece[][] board;
	private int[] bestMove;
	private int maxDepth = 0;
	private Timer timer;
	private int timeLeft = 6;

	private int[][] idsBestMove;
	private boolean[] depthDone;

	private int[] history;

	// Awesome print statements
	// System.out.println(gameBoard.toString());
	// System.out.println("Ply(" + depth + ") minScore= " + minScore);
	// System.out.println("Ply(" + depth + ") maxScore= " + maxScore);
	// System.out.println("Ply(" + depth + ") bestscore= " + bestScore);

	/**
	 * Gets the best score and best move for a computer
	 * 
	 * @param board
	 */
	public MiniMax(Board board) {
		// Setup timer
		ActionListener timerAction = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (timeLeft == 0) {
					timer.stop();
				} else {
					timeLeft--;
					System.out.println("TimeLeft: " + timeLeft);
				}

			}
		};
		timer = new Timer(1000, timerAction);
		timer.setInitialDelay(0);
		timer.start();

		// Get game board variables
		this.gameBoard = board;
		this.board = gameBoard.getBoard();

		// Create history table
		history = new int[7777];
		Arrays.fill(history, 0);

		// Fill depth done with all false
		depthDone = new boolean[101];
		Arrays.fill(depthDone, false);

		// Initialize idsBestMove
		idsBestMove = new int[101][4];

		// Iterative Deepening
		while (timeLeft > 0) {

			Vector<?> validMoves;
			int bestScore = -100000;
			int depth = 0;
			maxDepth++;

			// Get all valid moves
			validMoves = this.gameBoard.getComputer().generateMoves(this.gameBoard.getBoard());

			// Sort all valid moves based on history table
			sortValidMoves(validMoves);

			// For every valid move
			Iterator<?> iterator = validMoves.iterator();
			while (iterator.hasNext()) {
				int[] i = (int[]) iterator.next();
				Piece piece = this.board[i[1]][i[0]];
				// 1. Save move
				int backupGas = piece.getGas();
				Piece eatenPiece = null;
				// 2. Make move
				// - Returns the eaten piece if any
				eatenPiece = move(i);

				// 3. Get best move from Min
				int minScore = Min(depth + 1, bestScore, this.bestMove);

				// 4. If move from Min > bestScore, replace
				if (minScore > bestScore) {
					bestScore = minScore;
					bestMove = i;
				}

				// 5. Retract move
				if (eatenPiece != null) {
					// Recover Collision
					revertMove(i, backupGas, eatenPiece);
				} else {
					// Recover Move
					revertMove(i, backupGas);
				}
			}

			if (timeLeft > 0) {
				if (maxDepth > 99) {
					timer.stop();
					timeLeft = -1;
					break;
				}
				idsBestMove[maxDepth] = bestMove;
				depthDone[maxDepth] = true;
				System.out.println("Depth " + maxDepth + " done (" + bestScore + ")");
			}

			if (depthDone[maxDepth] == false) {
				bestMove = idsBestMove[maxDepth - 1];
			}
		}
	}

	/*********************************************************************************************
	 * Max
	 ********************************************************************************************/
	/**
	 * Gets the best score and best move for a computer
	 * 
	 * @param depth
	 * @param score
	 * @param bestMove
	 * @return
	 */
	private int Max(int depth, int score, int[] bestMove) {
		int bestScore = -100000;

		Vector<?> validMoves;

		// If time is over return
		if (timeLeft <= 0) {
			// System.out.println("Ply(" + depth + ") score= " + score);
			return 100000;
		}

		// Check if game is over
		if (checkGameOver())
			return -100000 + depth;

		// If @ max depth, do EVAL
		if (depth == maxDepth)
			return evaluation();

		// Else Generate all valid moves
		validMoves = this.gameBoard.getComputer().generateMoves(this.gameBoard.getBoard());

		// For every valid move
		Iterator<?> iterator = validMoves.iterator();

		while (iterator.hasNext()) {
			int[] i = (int[]) iterator.next();
			Piece piece = this.board[i[1]][i[0]];

			// 1. Save move
			int backupGas = piece.getGas();
			Piece eatenPiece = null;

			// 2. Make move
			// - Returns the eaten piece if any
			eatenPiece = move(i);

			// 3. Get best move from Min
			int minScore = Min(depth + 1, bestScore, this.bestMove);

			// 4. If move from Min > bestScore, replace
			if (minScore > bestScore) {
				bestScore = minScore;
				bestMove = i;
			}

			// 5. Retract move
			if (eatenPiece != null) {
				// Recover Collision
				revertMove(i, backupGas, eatenPiece);
			} else {
				// Recover Move
				revertMove(i, backupGas);
			}

			// 6. Alpha Beta Pruning for Max
			if (minScore > score) {
				// 7. Increment bestMove in history list
				updateHistory(bestMove);
				break;
			}
		}
		// Return bestMove, bestScore
		return bestScore;
	}

	/*********************************************************************************************
	 * Min
	 ********************************************************************************************/
	/**
	 * Gets the best score and best move for a human
	 * 
	 * @param depth
	 * @param score
	 * @param bestMove
	 * @return
	 */
	private int Min(int depth, int score, int[] bestMove) {
		int bestScore = 100000;

		Vector<?> validMoves;

		// If time is over return
		if (timeLeft <= 0) {
			// System.out.println("Ply(" + depth + ") score= " + score);
			return -100000;
		}

		// Check if game is over
		if (checkGameOver())
			return 100000 - depth;

		// If @ max depth, do EVAL
		if (depth == maxDepth)
			return evaluation();

		// Else Generate all valid moves
		validMoves = this.gameBoard.getHuman().generateMoves(this.gameBoard.getBoard());

		// For every valid move
		Iterator<?> iterator = validMoves.iterator();

		while (iterator.hasNext()) {
			int[] i = (int[]) iterator.next();
			Piece piece = this.board[i[1]][i[0]];

			// 1. Save move
			int backupGas = piece.getGas();
			Piece eatenPiece = null;

			// 2. Make move
			// - Returns the eaten piece if any
			eatenPiece = move(i);

			// 3. Get best move from Max
			int maxScore = Max(depth + 1, bestScore, this.bestMove);

			// 4. If move from Max < bestScore, replace
			if (maxScore < bestScore) {
				bestScore = maxScore;
				bestMove = i;
			}

			// 5. Retract move
			if (eatenPiece != null) {
				// Recover Collision
				revertMove(i, backupGas, eatenPiece);
			} else {
				// Recover Move
				revertMove(i, backupGas);
			}

			// 6. Alpha Beta Pruning for Max
			if (maxScore < score) {
				// 7. Increment bestMove in history list
				updateHistory(bestMove);
				break;
			}
		}
		// Return bestMove, bestScore
		return bestScore;
	}

	/*********************************************************************************************
	 * EVAL
	 ********************************************************************************************/
	/**
	 * Heuristic Function
	 * <p>
	 * Secret formula
	 * 
	 * @return
	 */
	private int evaluation() {
		int humanGas = 0;
		int computerGas = 0;
		int score = 0;
		// 1. Human total gas vs Computer total gas
		// - gas difference * 3
		// - King has gas >= 1, 5 more points
		// - Queen has gas >= 1, 2 more point

		// 2. Number of Human pieces vs Number of Computer pieces
		// - Queen: 7 pts and 2 more points if has gas
		// - Knight: 3 pts if has gas
		// - Bishop: 3 pts if has gas

		Iterator<?> iterator = this.gameBoard.getHuman().getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			humanGas -= piece.getGas();
			if (piece instanceof Queen) {
				score -= 7;
				if (piece.getGas() >= 1)
					score -= 2;
			}
			if (piece instanceof Knight) {
				if (piece.getGas() >= 1)
					score -= 3;
			}
			if (piece instanceof Bishop) {
				if (piece.getGas() >= 1)
					score -= 3;
			}
			if (piece instanceof King) {
				if (piece.getGas() >= 1)
					score -= 5;
			}
		}

		iterator = this.gameBoard.getComputer().getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			humanGas += piece.getGas();
			if (piece instanceof Queen) {
				score += 7;
				if (piece.getGas() >= 1)
					score += 2;
			}
			if (piece instanceof Knight) {
				if (piece.getGas() >= 1)
					score += 3;
			}
			if (piece instanceof Bishop) {
				if (piece.getGas() >= 1)
					score += 3;
			}
			if (piece instanceof King) {
				if (piece.getGas() >= 1)
					score += 5;
			}
		}

		score += ((humanGas + computerGas) * 3);

		// System.out.println(i);
		return score;
	}

	/*********************************************************************************************
	 * Helper functions
	 ********************************************************************************************/
	/**
	 * Moves a piece on the board
	 * 
	 * @param i
	 * @return
	 */
	public Piece move(int[] i) {
		// Separate move into ints
		int curX = i[0];
		int curY = i[1];
		int destX = i[2];
		int destY = i[3];
		Piece eatenPiece = null;

		board[curY][curX].move(gameBoard.getPositionXChar(destX), gameBoard.getPositionYInt(destY));

		// Handle Collision
		// Collision
		if (board[destY][destX] != null) {
			// Save the eaten piece
			eatenPiece = board[destY][destX];

			// Remove the eaten piece
			char name = board[destY][destX].getName().charAt(0);

			// If name is lowercase, then it is a player piece
			if (Character.isLowerCase(name)) {
				Iterator<?> iter = gameBoard.getHuman().getIterator();
				Piece piece = null;
				while (iter.hasNext()) {
					piece = (Piece) iter.next();
					if (piece == board[destY][destX]) {
						break;
					}
				}
				gameBoard.getHuman().getPlayerPieces().remove(piece);
			} else if (Character.isUpperCase(name)) {
				Iterator<?> iter = gameBoard.getComputer().getIterator();
				Piece piece = null;
				while (iter.hasNext()) {
					piece = (Piece) iter.next();
					if (piece == board[destY][destX]) {
						break;
					}
				}
				gameBoard.getComputer().getPlayerPieces().remove(piece);
			}

			// Print move
			// gameBoard.printMove(i);

			// Print inverse of the move
			// gameBoard.printInverseMove(i);

			// Move piece to destination
			board[destY][destX] = board[curY][curX];
			board[curY][curX] = null;

			// Increase gas of eater to 3
			board[destY][destX].setGas(3);

			return eatenPiece;
		}
		// No Collision
		else {
			// Print move
			// gameBoard.printMove(i);

			// Print inverse of the move
			// gameBoard.printInverseMove(i);

			board[destY][destX] = board[curY][curX];
			board[curY][curX] = null;

			return eatenPiece;
		}
	}

	/**
	 * Reverts a move w/o a collision
	 * 
	 * @param i
	 * @param backupGas
	 */
	private void revertMove(int[] i, int backupGas) {
		// Separate move into ints
		int curX = i[2];
		int curY = i[3];
		int destX = i[0];
		int destY = i[1];

		// Update Piece values x, y
		board[curY][curX].move(gameBoard.getPositionXChar(destX), gameBoard.getPositionYInt(destY));

		// Update board state
		board[destY][destX] = board[curY][curX];
		board[curY][curX] = null;

		// Revert gas
		board[destY][destX].setGas(backupGas);
	}

	/**
	 * Reverts a move w/ a collision
	 * 
	 * @param i
	 * @param backupGas
	 * @param eatenPiece
	 */
	private void revertMove(int[] i, int backupGas, Piece eatenPiece) {
		// Separate move into ints
		int curX = i[2];
		int curY = i[3];
		int destX = i[0];
		int destY = i[1];

		// Update Piece values x, y
		board[curY][curX].move(gameBoard.getPositionXChar(destX), gameBoard.getPositionYInt(destY));

		// Update board state
		board[destY][destX] = board[curY][curX];
		board[curY][curX] = null;

		// Revert gas
		board[destY][destX].setGas(backupGas);

		// Recover eatenPiece
		// Human Piece
		if (Character.isLowerCase(eatenPiece.getName().charAt(0))) {
			// Give piece back to human
			gameBoard.getHuman().getPlayerPieces().add(eatenPiece);

			gameBoard.addToBoard(eatenPiece); // Add piece back to board
		}
		// Computer Piece
		if (Character.isUpperCase(eatenPiece.getName().charAt(0))) {
			// Give piece back to computer
			gameBoard.getComputer().getPlayerPieces().add(eatenPiece);
			gameBoard.addToBoard(eatenPiece); // Add piece back to board
		}

	}

	/**
	 * Updates the history table with the move that recently pruned
	 * 
	 * @param bestMove
	 */
	private void updateHistory(int[] bestMove) {
		String moveString = bestMove[0] + "" + bestMove[1] + "" + bestMove[2] + "" + bestMove[3];
		history[Integer.parseInt(moveString)]++;
		// System.out.println(moveString + ": " +
		// history[Integer.parseInt(moveString)]);
	}

	/**
	 * Sorts the valid moves based on history table
	 * 
	 * @param validMoves
	 */
	private void sortValidMoves(Vector validMoves) {
		int size = validMoves.size();
		int[] moveInt = (int[]) validMoves.elementAt(0);
		String moveString = moveInt[0] + "" + moveInt[1] + "" + moveInt[2] + "" + moveInt[3];
		for (int i = 1; i < size; i++) {
			moveInt = (int[]) validMoves.elementAt(i);
			moveString = moveInt[0] + "" + moveInt[1] + "" + moveInt[2] + "" + moveInt[3];

			for (int j = 0; j < i; j++) {
				int[] moveInt2 = (int[]) validMoves.elementAt(j);
				String moveString2 = moveInt2[0] + "" + moveInt2[1] + "" + moveInt2[2] + "" + moveInt2[3];
				if (history[Integer.parseInt(moveString)] > history[Integer.parseInt(moveString2)]) {
					validMoves.remove(moveInt);
					validMoves.insertElementAt(moveInt, 0);
					break;
				}
			}
		}
		// System.out.println("Depth " + maxDepth + " size: " + size);
	}

	/*********************************************************************************************
	 * Gameover functions
	 ********************************************************************************************/
	/**
	 * Check if the game is over
	 * <p>
	 * To win
	 * <p>
	 * 1. Kill Oponent's King
	 * <p>
	 * 2. All of Oponent's pieces have 0 gas
	 * 
	 * @return
	 */
	private boolean checkGameOver() {
		if (check4KingHuman() <= -5000) {
			// System.out.println("Human win hit");
			return true;
		}
		if (check4KingComputer() >= 5000) {
			// System.out.println("Computer win hit");
			return true;
		}
		if (check4Gas() <= -5000) {
			// System.out.println("Human Gas win hit");
			return true;
		}
		if (check4Gas() >= 5000) {
			// System.out.println("Computer Gas win hit");
			return true;
		}
		return false;
	}

	/**
	 * Check if human does not have a king
	 * 
	 * @param playerType
	 * @return
	 */
	private int check4KingHuman() {
		Iterator<?> iterator;

		// check
		iterator = gameBoard.getHuman().getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			if (piece instanceof King) {
				return 0; // Game not over
			}
		}

		return -500000;

	}

	/**
	 * Check if computer does not have a king
	 * 
	 * @param playerType
	 * @return
	 */
	private int check4KingComputer() {
		Iterator<?> iterator;

		// check
		iterator = gameBoard.getComputer().getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			if (piece instanceof King) {
				return 0; // Game not over
			}
		}
		return 500000;

	}

	/**
	 * Check if either the human or computer has no gas in any of its pieces
	 * 
	 * @return
	 */
	private int check4Gas() {
		Iterator<?> iterator;

		// All of Oponent's pieces have 0 gas
		int gasTotal = 0;
		// No gas for human, computer wins
		iterator = gameBoard.getHuman().getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			gasTotal += piece.getGas();
		}

		if (gasTotal == 0) {
			return 500000; // human no gas, computer wins
		}
		// No gas for computer, human wins
		gasTotal = 0;
		iterator = gameBoard.getComputer().getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			gasTotal += piece.getGas();
		}

		if (gasTotal == 0) {
			return -500000; // computer no gas, human wins
		}

		return 0; // Game not over
	}

	/*********************************************************************************************
	 * Getters and Setters
	 ********************************************************************************************/
	public int[] getBestMove() {
		return bestMove;
	}
}
