package game;

import game.board.Board;
import game.board.pieces.King;
import game.board.pieces.Piece;

import java.util.Iterator;
import java.util.Scanner;

/**
 * Main Class that handles the Gas Tank Chess game. Handles the main game loop.
 * 
 * @author Anthony Nguyen
 *
 */
public class GasTankChessMain {

	// Variables
	private Board board;
	private int currentTurn;

	private Scanner scanner;

	// Constants
	private final int HUMAN_TURN = 1;
	private final int COMPUTER_TURN = 0;

	public GasTankChessMain() {

		initGame();

		while (!checkGameOver()) {
			if (currentTurn == HUMAN_TURN)
				getPlayerMove();
			if (checkGameOver())
				break;
			if (currentTurn == COMPUTER_TURN)
				getComputerMove();
			if (checkGameOver())
				break;
		}
	}

	/**
	 * Initializes GasTank Chess
	 */
	private void initGame() {
		scanner = new Scanner(System.in);

		initBoard();

	}

	/**
	 * Initialize the board and determine who gets the first turn
	 */
	private void initBoard() {
		String answer;
		System.out.printf(" Program Name: Up & Down\n Programmer  : Anthony Nguyen\n Spring 2015\n CSC 180\n GasTank Chess\n\n");
		// Who wants to go first?
		System.out.printf("Do you want to go first? (0/1)\n  (0) No\n  (1) Yes\nAnswer: ");
		while (true) {
			answer = scanner.nextLine();
			// Successfully got answer
			if (answer.equals("0")) {
				System.out.println("Okay I will go first then.");
				currentTurn = 0;
				break;
			}
			if (answer.equals("1")) {
				System.out.println("Okay you will go first then.");
				currentTurn = 1;
				break;
			}
			System.out.printf("Do you want to go first? (0/1)\n  (0) No\n  (1) Yes\nAnswer: ");
		}

		board = new Board();
		System.out.println(board);
	}

	/**
	 * The player will input a move
	 */
	private void getPlayerMove() {
		// Get player move request
		scanner = new Scanner(System.in);
		System.out.printf("Your move: ");
		while (board.humanMove(scanner.nextLine()) == false) {
			System.out.println("**Try Again**");
			System.out.printf("Your move: ");
		}
		this.currentTurn = COMPUTER_TURN;
	}

	/**
	 * The computer will do a move
	 */
	private void getComputerMove() {
		board.computerMove();
		this.currentTurn = HUMAN_TURN;
	}

	/**
	 * Check if the game is over
	 * <p>
	 * To win
	 * <p>
	 * 1. Kill Oponent's King
	 * <p>
	 * 2. All of Oponent's pieces have 0 gas
	 * 
	 * @return
	 */
	private boolean checkGameOver() {
		if (check4King(this.COMPUTER_TURN) <= -5000) {
			System.out.println("You Win!");
			return true;
		} else if (check4King(this.HUMAN_TURN) >= 5000) {
			System.out.println("Computer Wins!");
			return true;
		} else if (check4Gas() <= -5000) {
			System.out.println("You Win!");
			return true;
		} else if (check4Gas() >= 5000) {
			System.out.println("Computer Wins!");
			return true;
		} else
			return false;
	}

	/**
	 * Check if a human or computer does not have a king
	 * 
	 * @param playerType
	 * @return
	 */
	private int check4King(int playerType) {
		Iterator<?> iterator;

		// check
		if (playerType == this.HUMAN_TURN)
			iterator = board.getHuman().getIterator();
		else
			iterator = board.getComputer().getIterator();

		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			if (piece instanceof King) {
				return 0; // Game not over
			}
		}

		if (playerType == this.HUMAN_TURN)
			return 500000; // human turn, no king, computer wins
		else
			return -500000; // computer turn, no king, human wins
	}

	/**
	 * Check if either the human or computer has no gas in any of its pieces
	 * 
	 * @return
	 */
	private int check4Gas() {
		Iterator<?> iterator;

		// All of Oponent's pieces have 0 gas
		int gasTotal = 0;
		// No gas for human, computer wins
		iterator = board.getHuman().getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			gasTotal += piece.getGas();
		}

		if (gasTotal == 0) {
			return 500000; // human no gas, computer wins
		}
		// No gas for computer, human wins
		gasTotal = 0;
		iterator = board.getComputer().getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			gasTotal += piece.getGas();
		}

		if (gasTotal == 0) {
			return -500000; // computer no gas, human wins
		}

		return 0; // Game not over
	}
}
