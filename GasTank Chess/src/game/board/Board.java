package game.board;

import game.board.pieces.Piece;
import game.playerTypes.Computer;
import game.playerTypes.Human;

import java.util.Iterator;
import java.util.Vector;

import minimax.MiniMax;

/**
 * This is the game board for Gas Tank Chess
 * <p>
 * The Board acts as the "GameWorld" for this game
 * <p>
 * Most of the logic and game play is handled here
 * 
 * @author Anthony Nguyen
 *
 */
public class Board {

	// Variables
	private Piece[][] board;
	private Human human;
	private Computer computer;

	// Constants
	private final int PLAYER_STARTING_ROW = 1;
	private final int COMPUTER_STARTING_ROW = 7;

	public Board() {
		// Create Board
		board = new Piece[7][8];

		// Create Players
		this.human = new Human(PLAYER_STARTING_ROW);
		this.computer = new Computer(COMPUTER_STARTING_ROW);

		// Add pieces to board
		Iterator<Piece> iterator = human.getIterator();
		while (iterator.hasNext()) {
			addToBoard(iterator.next());
		}

		iterator = computer.getIterator();
		while (iterator.hasNext()) {
			addToBoard(iterator.next());
		}
	}

	/**
	 * Handles movement for human player
	 * 
	 * @param movement
	 * @return
	 */
	public boolean humanMove(String movement) {
		System.out.println("***Human******************************************************************");

		// Parse String from user/computer to Array[][] structure
		if (movement.length() == 4) {
			int curX = parseString(movement, 0);
			int curY = parseString(movement, 1);
			int destX = parseString(movement, 2);
			int destY = parseString(movement, 3);
			if (curX == -1 || curY == -1 || destX == -1 || destY == -1)
				return false;
			// System.out.println("Move:");
			// System.out.println(curX + ", " + curY + ", " + destX + ", " +
			// destY);

			// Check if that piece exists and if the move is valid
			if (board[curY][curX] != null) {
				// System.out.println(board[curY][curX] + " exists");

				// Generate moves for that piece and see if the move is valid
				Vector<int[]> validMoves = new Vector<int[]>();
				validMoves.addAll(human.generateMoves(board));

				Iterator<int[]> iterator = validMoves.iterator();
				// System.out.println("Valid Moves:");
				while (iterator.hasNext()) {
					int[] i = (int[]) iterator.next();
					// System.out.println(i[0] + ", " + i[1] + ", " + i[2] +
					// ", " + i[3]);

					// Check if the move is valid or not

					// Valid
					if (curX == i[0] && curY == i[1] && destX == i[2] && destY == i[3]) {
						board[curY][curX].move(getPositionXChar(i[2]), getPositionYInt(i[3]));

						// Handle Collision
						// Collision
						if (board[destY][destX] != null) {
							// Remove the eaten piece
							char name = board[destY][destX].getName().charAt(0);

							// If name is lowercase, then it is a player piece
							if (Character.isLowerCase(name)) {
								Iterator<?> iter = human.getIterator();
								Piece piece = null;
								while (iter.hasNext()) {
									piece = (Piece) iter.next();
									if (piece == board[destY][destX]) {
										break;
									}
								}
								human.getPlayerPieces().remove(piece);
							} else if (Character.isUpperCase(name)) {
								Iterator<?> iter = computer.getIterator();
								Piece piece = null;
								while (iter.hasNext()) {
									piece = (Piece) iter.next();
									if (piece == board[destY][destX]) {
										break;
									}
								}
								computer.getPlayerPieces().remove(piece);
							}

							// Print move
							printMove(i);

							// Print inverse of the move
							printInverseMove(i);

							// Move piece to destination
							board[destY][destX] = board[curY][curX];
							board[curY][curX] = null;

							// Increase gas of eater to 3
							board[destY][destX].setGas(3);

							System.out
									.println("**************************************************************************");

							// Reprint board
							System.out.println(this.toString());

							return true; // Valid move done, so return function
						}
						// No Collision
						else {
							// Print move
							printMove(i);

							// Print inverse of the move
							printInverseMove(i);

							board[destY][destX] = board[curY][curX];
							board[curY][curX] = null;

							System.out
									.println("**************************************************************************");

							// Reprint board
							System.out.println(this.toString());

							return true; // Valid move done, so return function
						}
					}
				}

				// Reprint board
				System.out.println(this.toString());
				// After going through all valid moves, and the move was not
				// found, print error
				System.out.println("Not a valid move for piece(" + movement.substring(0, 1) + movement.substring(1, 2)
						+ ")");

				System.out.println("**************************************************************************");

				// Did not move
				return false;
			}
			// Invalid Move
			else {
				// No piece in that space
				System.out.println("There is no piece at space " + movement.substring(0, 2));

				// Reprint board
				System.out.println(this.toString());

				System.out.println("**************************************************************************");

				// Did not move
				return false;
			}
		} else
			// Reprint board
			System.out.println(this.toString());
		System.out.println("**************************************************************************");

		// Did not move
		return false;
	}

	/**
	 * Computes computer move using minimax
	 */
	public void computerMove() {
		int curX;
		int curY;
		int destX;
		int destY;
		int[] i;

		System.out.println("***Computer***************************************************************");

		// Generate all computer moves
		Vector<int[]> validMoves = new Vector<int[]>();
		validMoves.addAll(computer.generateMoves(board));

		// Use minimax to compute best move
		MiniMax minimax = new MiniMax(this);

		// Get best move
		i = minimax.getBestMove();

		// Separate move into ints
		curX = i[0];
		curY = i[1];
		destX = i[2];
		destY = i[3];

		board[curY][curX].move(getPositionXChar(i[2]), getPositionYInt(i[3]));

		// Handle Collision
		// Collision
		if (board[destY][destX] != null) {
			// Remove the eaten piece
			char name = board[destY][destX].getName().charAt(0);

			// If name is lowercase, then it is a player piece
			if (Character.isLowerCase(name)) {
				Iterator<?> iter = human.getIterator();
				Piece piece = null;
				while (iter.hasNext()) {
					piece = (Piece) iter.next();
					if (piece == board[destY][destX]) {
						break;
					}
				}
				human.getPlayerPieces().remove(piece);
			} else if (Character.isUpperCase(name)) {
				Iterator<?> iter = computer.getIterator();
				Piece piece = null;
				while (iter.hasNext()) {
					piece = (Piece) iter.next();
					if (piece == board[destY][destX]) {
						break;
					}
				}
				computer.getPlayerPieces().remove(piece);
			}

			// Print move
			printMove(i);

			// Print inverse of the move
			printInverseMove(i);

			// Move piece to destination
			board[destY][destX] = board[curY][curX];
			board[curY][curX] = null;

			// Increase gas of eater to 3
			board[destY][destX].setGas(3);

			// Reprint board
			System.out.println(this.toString());

		}
		// No Collision
		else {
			// Print move
			printMove(i);

			// Print inverse of the move
			printInverseMove(i);

			board[destY][destX] = board[curY][curX];
			board[curY][curX] = null;

			// Reprint board
			System.out.println(this.toString());

		}

		System.out.println("**************************************************************************");
	}

	public void printMove(int[] i) {
		System.out.println("Move: " + getPositionXChar(i[0]) + getPositionYInt(i[1]) + getPositionXChar(i[2])
				+ getPositionYInt(i[3]));
	}

	public void printInverseMove(int[] i) {
		char invCurX = getPositionXChar(i[0]);
		int invCurY = i[1] + 1;
		char invDestX = getPositionXChar(i[2]);
		int invDestY = i[3] + 1;
		System.out.println("Inverse Move: " + invCurX + invCurY + invDestX + invDestY);
	}

	/**
	 * Parses string c#c# into 2D array values
	 * 
	 * @param s
	 * @param i
	 * @return
	 */
	private int parseString(String s, int i) {
		String stringInt = s.substring(i, i + 1);
		if (i == 0 || i == 2) {
			switch (stringInt) {
			case "A":
				return 0;
			case "B":
				return 1;
			case "C":
				return 2;
			case "D":
				return 3;
			case "E":
				return 4;
			case "F":
				return 5;
			case "G":
				return 6;
			case "H":
				return 7;
			default:
				return -1;
			}
		} else if (i == 1 || i == 3) {
			switch (stringInt) {
			case "7":
				return 0;
			case "6":
				return 1;
			case "5":
				return 2;
			case "4":
				return 3;
			case "3":
				return 4;
			case "2":
				return 5;
			case "1":
				return 6;
			default:
				return -1;
			}
		}
		return -1;
	}

	/**
	 * Add a piece to the board
	 * 
	 * @param piece
	 */
	public void addToBoard(Piece piece) {
		int x = getPositionX(piece.getX());
		int y = getPositionY(piece.getY());
		board[y][x] = piece;
	}

	/**
	 * Get C position for a 2D array
	 * 
	 * @param c
	 * @return
	 */
	public int getPositionX(char c) {
		switch (c) {
		case 'A':
			return 0;
		case 'B':
			return 1;
		case 'C':
			return 2;
		case 'D':
			return 3;
		case 'E':
			return 4;
		case 'F':
			return 5;
		case 'G':
			return 6;
		case 'H':
			return 7;
		default:
			return -1;
		}
	}

	/**
	 * Get Y position for a 2D array
	 * 
	 * @param i
	 * @return
	 */
	public int getPositionY(int i) {
		switch (i) {
		case 7:
			return 0;
		case 6:
			return 1;
		case 5:
			return 2;
		case 4:
			return 3;
		case 3:
			return 4;
		case 2:
			return 5;
		case 1:
			return 6;
		default:
			return -1;
		}
	}

	/**
	 * Get X char for board view
	 * 
	 * @param i
	 * @return
	 */
	public char getPositionXChar(int i) {
		switch (i) {
		case 0:
			return 'A';
		case 1:
			return 'B';
		case 2:
			return 'C';
		case 3:
			return 'D';
		case 4:
			return 'E';
		case 5:
			return 'F';
		case 6:
			return 'G';
		case 7:
			return 'H';
		default:
			return '0';
		}
	}

	/**
	 * Get Y int for board view
	 * 
	 * @param i
	 * @return
	 */
	public int getPositionYInt(int i) {
		switch (i) {
		case 0:
			return 7;
		case 1:
			return 6;
		case 2:
			return 5;
		case 3:
			return 4;
		case 4:
			return 3;
		case 5:
			return 2;
		case 6:
			return 1;
		default:
			return -1;
		}
	}

	public String toString() {

		String string;
		string = "***** Current Board State *****\n";
		for (int i = 0; i <= 6; i++) {
			switch (i) {
			case 0:
				string += "7  ";
				break;
			case 1:
				string += "6  ";
				break;
			case 2:
				string += "5  ";
				break;
			case 3:
				string += "4  ";
				break;
			case 4:
				string += "3  ";
				break;
			case 5:
				string += "2  ";
				break;
			case 6:
				string += "1  ";
				break;
			default:
				break;
			}
			for (int j = 0; j <= 7; j++) {
				if (j == 7) {
					if (board[i][j] == null) {
						string += "--\n";
					} else {
						string += board[i][j] + "\n";
					}
				} else {
					if (board[i][j] == null) {
						string += "-- ";
					} else {
						string += board[i][j] + " ";
					}
				}
			}
		}

		string += "  ________________________\n";
		string += "   A  B  C  D  E  F  G  H\n";
		return string;

		/*
		 * String string; string = "***** Current Board State *****\n"; for (int
		 * i = 0; i <= 6; i++) { switch (i) { case 0: string += "0 7  "; break;
		 * case 1: string += "1 6  "; break; case 2: string += "2 5  "; break;
		 * case 3: string += "3 4  "; break; case 4: string += "4 3  "; break;
		 * case 5: string += "5 2  "; break; case 6: string += "6 1  "; break;
		 * default: break; } for (int j = 0; j <= 7; j++) { if (j == 7) { if
		 * (board[i][j] == null) { string += "--\n"; } else { string +=
		 * board[i][j] + "\n"; } } else { if (board[i][j] == null) { string +=
		 * "-- "; } else { string += board[i][j] + " "; } } } }
		 * 
		 * string += "    ________________________\n"; string +=
		 * "     A  B  C  D  E  F  G  H\n"; string +=
		 * "     0  1  2  3  4  5  6  7\n"; return string;
		 */
	}

	/*********************************************************************************************
	 * Getters and Setters
	 ********************************************************************************************/

	public Piece[][] getBoard() {
		return board;
	}

	public Human getHuman() {
		return human;
	}

	public Computer getComputer() {
		return computer;
	}

}
