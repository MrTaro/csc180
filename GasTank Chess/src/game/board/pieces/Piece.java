package game.board.pieces;


/**
 * A chess piece that manages a piece's coordinates, amount of gas, and its name
 * 
 * @author Anthony Nguyen
 *
 */
public class Piece {
	// Variables
	private int gas;
	private char x;
	private int y;
	private String name;

	public Piece(char x, int y, String name) {
		this.x = x;
		this.y = y;
		this.gas = 3;
		this.name = name;
	}

	public void move(char x, int y) {
		this.x = x;
		this.y = y;
		this.gas--;
	}

	public boolean hasGas() {
		if (gas > 0)
			return true;
		else
			return false;
	}

	public String toString() {
		String string = name + gas;
		return string;
	}

	/*********************************************************************************************
	 * Getters and Setters
	 ********************************************************************************************/

	public char getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(char x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getGas() {
		return gas;
	}

	public void setGas(int gas) {
		this.gas = gas;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
