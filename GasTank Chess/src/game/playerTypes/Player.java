package game.playerTypes;

import game.board.pieces.Bishop;
import game.board.pieces.King;
import game.board.pieces.Knight;
import game.board.pieces.Piece;
import game.board.pieces.Queen;

import java.util.Iterator;
import java.util.Vector;

/**
 * Player class handles both of the Human and Computer's functions
 * @author Anthony Nguyen
 *
 */
public class Player {
	private Vector<Piece> playerPieces;

	public Player(int startingRow) {
		playerPieces = new Vector<Piece>();
	}

	/**
	 * Generate all moves for all pieces for this player
	 * 
	 * @param board
	 * @return
	 */
	public Vector<int[]> generateMoves(Piece[][] board) {
		Vector<int[]> moves = new Vector<int[]>();

		// Get all moves from all pieces
		moves.addAll(generateKingMoves());
		moves.addAll(generateQueenMoves(board));
		moves.addAll(generateKnightMoves());
		moves.addAll(generateBishopMoves(board));

		return moves;
	}

	/**
	 * Generate king moves
	 * 
	 * @return
	 */
	public Vector<int[]> generateKingMoves() {
		Vector<int[]> moves = new Vector<int[]>();

		King king = null;

		// Get the only king
		Iterator<Piece> iterator = playerPieces.iterator();
		while (iterator.hasNext()) {
			Object piece = iterator.next();
			if (piece instanceof King) {
				king = (King) piece;
			}
		}

		// Make sure there is a king
		if (king != null) {
			// Make sure king has gas
			if (king.hasGas()) {
				// System.out.println("King Exists for player!");
				int x = getPositionX(king.getX());
				int y = getPositionY(king.getY());

				// Generate moves starting with moving the piece up and going
				// clockwise
				// Move up
				if (y - 1 >= 0) {
					int[] i = { x, y, x, y - 1 };
					// System.out.println(i[0] + ", " + i[1] + ", " + i[2] +
					// ", " + i[3]);
					moves.add(i);
				}

				// Move top right
				if (x + 1 <= 7 && y - 1 >= 0) {
					int[] i = { x, y, x + 1, y - 1 };
					moves.add(i);
				}

				// Move right
				if (x + 1 <= 7) {
					int[] i = { x, y, x + 1, y };
					moves.add(i);
				}

				// Move bottom right
				if (x + 1 <= 7 && y + 1 <= 6) {
					int[] i = { x, y, x + 1, y + 1 };
					moves.add(i);
				}

				// Move bottom
				if (y + 1 <= 6) {
					int[] i = { x, y, x, y + 1 };
					moves.add(i);
				}

				// Move bottom left
				if (x - 1 >= 0 && y + 1 <= 6) {
					int[] i = { x, y, x - 1, y + 1 };
					moves.add(i);
				}

				// Move Left
				if (x - 1 >= 0) {
					int[] i = { x, y, x - 1, y };
					moves.add(i);
				}

				// Move top left
				if (x - 1 >= 0 && y - 1 >= 0) {
					int[] i = { x, y, x - 1, y - 1 };
					moves.add(i);
				}

			}
		}

		return moves;
	}

	/**
	 * Generate queen moves
	 * 
	 * @param board
	 * @return
	 */
	public Vector<int[]> generateQueenMoves(Piece[][] board) {
		Vector<int[]> moves = new Vector<int[]>();

		Queen queen = null;

		// Get the only king
		Iterator<Piece> iterator = playerPieces.iterator();
		while (iterator.hasNext()) {
			Object piece = iterator.next();
			if (piece instanceof Queen) {
				queen = (Queen) piece;
			}
		}

		// Make sure there is a queen
		if (queen != null) {
			if (queen.hasGas()) {
				// System.out.println("Queen Exists for player!");
				int x = getPositionX(queen.getX());
				int y = getPositionY(queen.getY());

				// Generate moves similar to knight and bishop

				// Knight Moves
				// Move up 2, right 1
				if (y - 2 >= 0 && x + 1 <= 7) {
					int[] i = { x, y, x + 1, y - 2 };
					if (!kingAt(i))
						moves.add(i);
				}

				// Move up 1, right 2
				if (y - 1 >= 0 && x + 2 <= 7) {
					int[] i = { x, y, x + 2, y - 1 };
					if (!kingAt(i))
						moves.add(i);
				}

				// Move down 1, right 2
				if (y + 1 <= 6 && x + 2 <= 7) {
					int[] i = { x, y, x + 2, y + 1 };
					if (!kingAt(i))
						moves.add(i);
				}

				// Move down 2, right 1
				if (y + 2 <= 6 && x + 1 <= 7) {
					int[] i = { x, y, x + 1, y + 2 };
					if (!kingAt(i))
						moves.add(i);
				}

				// Move down 2, left 1
				if (y + 2 <= 6 && x - 1 >= 0) {
					int[] i = { x, y, x - 1, y + 2 };
					if (!kingAt(i))
						moves.add(i);
				}

				// Move down 1, left 2
				if (y + 1 <= 6 && x - 2 >= 0) {
					int[] i = { x, y, x - 2, y + 1 };
					if (!kingAt(i))
						moves.add(i);
				}

				// Move up 1, left 2
				if (y - 1 >= 0 && x - 2 >= 0) {
					int[] i = { x, y, x - 2, y - 1 };
					if (!kingAt(i))
						moves.add(i);
				}

				// Move up 2, left 1
				if (y - 2 >= 0 && x - 1 >= 0) {
					int[] i = { x, y, x - 1, y - 2 };
					if (!kingAt(i))
						moves.add(i);
				}

				// Bishop Moves
				int j = 1;
				// Move up right
				while (x + j <= 7 && y - j >= 0) {
					int[] i = { x, y, x + j, y - j };
					if (!kingAt(i))
						moves.add(i);
					if (board[y - j][x + j] != null)
						break;
					j++;
				}

				// Move down right
				j = 1;
				while (x + j <= 7 && y + j <= 6) {
					int[] i = { x, y, x + j, y + j };
					if (!kingAt(i))
						moves.add(i);
					if (board[y + j][x + j] != null)
						break;
					j++;
				}

				// Move down left
				j = 1;
				while (x - j >= 0 && y + j <= 6) {
					int[] i = { x, y, x - j, y + j };
					if (!kingAt(i))
						moves.add(i);
					if (board[y + j][x - j] != null)
						break;
					j++;
				}

				// Move up left
				j = 1;
				while (x - j >= 0 && y - j >= 0) {
					int[] i = { x, y, x - j, y - j };
					if (!kingAt(i))
						moves.add(i);
					if (board[y - j][x - j] != null)
						break;
					j++;
				}
			}
		}

		return moves;
	}

	/**
	 * Generate knight moves
	 * 
	 * @return
	 */
	public Vector<int[]> generateKnightMoves() {
		Vector<int[]> moves = new Vector<int[]>();

		Knight knight = null;

		// Get a knight to get valid moves for that piece
		Iterator<Piece> iterator = playerPieces.iterator();
		while (iterator.hasNext()) {
			Object piece = iterator.next();
			if (piece instanceof Knight) {
				knight = (Knight) piece;

				// Make sure knight has gas
				if (knight.hasGas()) {
					// System.out.println("Knight Exists for player!");
					int x = getPositionX(knight.getX());
					int y = getPositionY(knight.getY());

					// Generate valid moves for knight starting by going 2
					// spaces up
					// and right
					// Move up 2, right 1
					if (y - 2 >= 0 && x + 1 <= 7) {
						int[] i = { x, y, x + 1, y - 2 };
						if (!kingAt(i))
							moves.add(i);
					}

					// Move up 1, right 2
					if (y - 1 >= 0 && x + 2 <= 7) {
						int[] i = { x, y, x + 2, y - 1 };
						if (!kingAt(i))
							moves.add(i);
					}

					// Move down 1, right 2
					if (y + 1 <= 6 && x + 2 <= 7) {
						int[] i = { x, y, x + 2, y + 1 };
						if (!kingAt(i))
							moves.add(i);
					}

					// Move down 2, right 1
					if (y + 2 <= 6 && x + 1 <= 7) {
						int[] i = { x, y, x + 1, y + 2 };
						if (!kingAt(i))
							moves.add(i);
					}

					// Move down 2, left 1
					if (y + 2 <= 6 && x - 1 >= 0) {
						int[] i = { x, y, x - 1, y + 2 };
						if (!kingAt(i))
							moves.add(i);
					}

					// Move down 1, left 2
					if (y + 1 <= 6 && x - 2 >= 0) {
						int[] i = { x, y, x - 2, y + 1 };
						if (!kingAt(i))
							moves.add(i);
					}

					// Move up 1, left 2
					if (y - 1 >= 0 && x - 2 >= 0) {
						int[] i = { x, y, x - 2, y - 1 };
						if (!kingAt(i))
							moves.add(i);
					}

					// Move up 2, left 1
					if (y - 2 >= 0 && x - 1 >= 0) {
						int[] i = { x, y, x - 1, y - 2 };
						if (!kingAt(i))
							moves.add(i);
					}
				}
			}
		}

		return moves;
	}

	/**
	 * Generates bishop moves
	 * 
	 * @param board
	 * @return
	 */
	public Vector<int[]> generateBishopMoves(Piece[][] board) {
		Vector<int[]> moves = new Vector<int[]>();

		Bishop bishop = null;

		// Get a bishop to get valid moves for that piece
		Iterator<Piece> iterator = playerPieces.iterator();
		while (iterator.hasNext()) {
			Object piece = iterator.next();
			if (piece instanceof Bishop) {
				bishop = (Bishop) piece;

				// Make sure bishop has gas
				if (bishop.hasGas()) {
					// System.out.println("Bishop Exists for player!");
					int x = getPositionX(bishop.getX());
					int y = getPositionY(bishop.getY());

					// Generate valid moves starting by going up right, will
					// stop
					// when out of board or
					// capture a piece
					int j = 1;
					// Move up right
					while (x + j <= 7 && y - j >= 0) {
						int[] i = { x, y, x + j, y - j };
						if (!kingAt(i))
							moves.add(i);
						if (board[y - j][x + j] != null)
							break;
						j++;
					}

					// Move down right
					j = 1;
					while (x + j <= 7 && y + j <= 6) {
						int[] i = { x, y, x + j, y + j };
						if (!kingAt(i))
							moves.add(i);
						if (board[y + j][x + j] != null)
							break;
						j++;
					}

					// Move down left
					j = 1;
					while (x - j >= 0 && y + j <= 6) {
						int[] i = { x, y, x - j, y + j };
						if (!kingAt(i))
							moves.add(i);
						if (board[y + j][x - j] != null)
							break;
						j++;
					}

					// Move up left
					j = 1;
					while (x - j >= 0 && y - j >= 0) {
						int[] i = { x, y, x - j, y - j };
						if (!kingAt(i))
							moves.add(i);
						if (board[y - j][x - j] != null)
							break;
						j++;
					}
				}
			}
		}

		return moves;
	}

	/*********************************************************************************************
	 * Helper Functions
	 ********************************************************************************************/

	/**
	 * Get C position for a 2D array
	 * 
	 * @param c
	 * @return
	 */
	private int getPositionX(char c) {
		switch (c) {
		case 'A':
			return 0;
		case 'B':
			return 1;
		case 'C':
			return 2;
		case 'D':
			return 3;
		case 'E':
			return 4;
		case 'F':
			return 5;
		case 'G':
			return 6;
		case 'H':
			return 7;
		default:
			return -1;
		}
	}

	/**
	 * Get Y position for a 2D array
	 * 
	 * @param i
	 * @return
	 */
	private int getPositionY(int i) {
		switch (i) {
		case 7:
			return 0;
		case 6:
			return 1;
		case 5:
			return 2;
		case 4:
			return 3;
		case 3:
			return 4;
		case 2:
			return 5;
		case 1:
			return 6;
		default:
			return -1;
		}
	}

	/**
	 * Check for a king at a destination position from dest x,y
	 * 
	 * @param i
	 * @return
	 */
	private boolean kingAt(int[] i) {
		// Get King piece for this player
		Iterator<Piece> iterator = this.getIterator();
		while (iterator.hasNext()) {
			Piece piece = (Piece) iterator.next();
			if (piece instanceof King) {
				King king = (King) piece;
				// If king is at dest x,y, return true
				int kingX = this.getPositionX(king.getX());
				int kingY = this.getPositionY(king.getY());
				if (kingX == i[2] && kingY == i[3]) {
					// System.out.println("Cannot eat your own king you fool");
					return true;
				}
			}
		}
		return false; // return false if king is not at dest x,y
	}

	/*********************************************************************************************
	 * Getters and Setters
	 ********************************************************************************************/

	public Vector<Piece> getPlayerPieces() {
		return playerPieces;
	}

	public void setPlayerPieces(Vector<Piece> playerPieces) {
		this.playerPieces = playerPieces;
	}

	public Iterator<Piece> getIterator() {
		return playerPieces.iterator();
	}

}
