package game.playerTypes;

import game.board.pieces.Bishop;
import game.board.pieces.King;
import game.board.pieces.Knight;
import game.board.pieces.Queen;

/**
 * Human class extends Player class
 * 
 * @author Anthony Nguyen
 *
 */
public class Human extends Player {

	public Human(int startingRow) {
		super(startingRow);
		this.getPlayerPieces().add(new King('E', startingRow, "k"));
		this.getPlayerPieces().add(new Queen('D', startingRow, "q"));
		this.getPlayerPieces().add(new Knight('D', startingRow + 1, "n"));
		this.getPlayerPieces().add(new Knight('F', startingRow + 1, "n"));
		this.getPlayerPieces().add(new Bishop('C', startingRow, "b"));
		this.getPlayerPieces().add(new Bishop('F', startingRow, "b"));
	}

}
