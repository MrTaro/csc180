package game.playerTypes;

import game.board.pieces.Bishop;
import game.board.pieces.King;
import game.board.pieces.Knight;
import game.board.pieces.Queen;

/**
 * Computer class extends Player class
 * 
 * @author Anthony Nguyen
 *
 */
public class Computer extends Player {

	public Computer(int startingRow) {
		super(startingRow);
		this.getPlayerPieces().add(new King('E', startingRow, "K"));
		this.getPlayerPieces().add(new Queen('D', startingRow, "Q"));
		this.getPlayerPieces().add(new Knight('D', startingRow - 1, "N"));
		this.getPlayerPieces().add(new Knight('F', startingRow - 1, "N"));
		this.getPlayerPieces().add(new Bishop('C', startingRow, "B"));
		this.getPlayerPieces().add(new Bishop('F', startingRow, "B"));
	}

}
