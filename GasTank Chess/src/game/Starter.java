package game;

import java.util.Scanner;

/**
 * Creates the game and handles pausing the game when game is over
 * 
 * @author Anthony Nguyen
 *
 */
public class Starter {

	public static void main(String[] args) {
		new GasTankChessMain();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Press enter to continue...");
		scanner.nextLine();
		scanner.close();
	}

}
